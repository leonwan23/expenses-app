import React from "react";

import chroma from "chroma-js";
import * as d3 from "d3";
import moment from "moment";
import _ from "underscore";

const radiusScale = d3.scaleLinear().range([15, 50]);

const height = 600;
const margin = { left: 60, top: 20, right: 40, bottom: 20 };

const simulation = d3
  .forceSimulation()
  .alphaDecay(0.001)
  .velocityDecay(0.3)
  .force(
    "collide",
    d3.forceCollide().radius(d => d.radius + 10)
  )
  .force(
    "x",
    d3.forceX(d => d.focusX)
  )
  .force(
    "y",
    d3.forceY(d => d.focusY)
  )
  .stop();

class Categories extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    simulation.on("tick", this.forceTick);
  }

  componentDidMount() {
    const { categories } = this.props;
    this.container = d3.select("#categories");
    this.calculateData();
    this.renderLinks();
    this.renderCircles();

    simulation
      .nodes(categories)
      .alpha(0.9)
      .restart();
  }

  componentDidUpdate() {
    this.calculateData();
    this.renderLinks();
    this.renderCircles();

    simulation
      .nodes(this.categories)
      .alpha(0.9)
      .restart();
  }

  calculateData() {
    const { categories, width } = this.props;
    const radiusExtent = d3.extent(categories, category => category.total);
    radiusScale.domain(radiusExtent);

    this.categories = _.map(categories, category => {
      return Object.assign(category, {
        radius: radiusScale(category.total),
        focusX: width / 2,
        focusY: height / 3
      });
    });
  }

  renderLinks = () => {
    const { links } = this.props;
    this.lines = this.container.selectAll("line").data(links);

    //exit + remove
    this.lines.exit().remove();

    //enter + update
    this.lines = this.lines
      .enter()
      .insert("line", "g")
      .attr("stroke", "#666")
      .merge(this.lines);
  };

  renderCircles() {
    this.circles = this.container.selectAll("g").data(this.categories);

    //exit and remove
    this.circles.exit().remove();

    //enter
    const enter = this.circles.enter().append("g");
    enter
      .append("circle")
      .attr("fill", "#fff")
      .attr("stroke", "#666")
      .attr("stroke-width", 2);
    enter
      .append("text")
      .attr("text-anchor", "middle")
      .attr("dy", ".35em");

    //enter + update
    this.circles = enter.merge(this.circles);
    this.circles.selectAll("circle").attr("r", d => d.radius);
    this.circles.selectAll("text").text(d => d.name);
  }

  forceTick = () => {
    this.circles.attr("transform", d => "translate(" + [d.x, d.y] + ")");
    this.lines
      .attr("x1", d => d.source.x)
      .attr("x2", d => d.target.x)
      .attr("y1", d => d.source.y)
      .attr("y2", d => d.target.y);
  };

  render() {
    return <g id="categories" />;
  }
}

export default Categories;
