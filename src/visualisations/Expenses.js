import React from "react";

import chroma from "chroma-js";
import * as d3 from "d3";
import moment from "moment";
import _ from "underscore";

const daysOfWeekArray = [
  [0, "Sun"],
  [1, "Mon"],
  [2, "Tue"],
  [3, "Wed"],
  [4, "Thu"],
  [5, "Fri"],
  [6, "Sat"]
];

const radius = 7;
const margin = {
  left: 10,
  right: 10,
  top: 10,
  bottom: 10
};

const xScale = d3.scaleBand().domain(_.map(daysOfWeekArray, 0));
const yScale = d3.scaleLinear();
const colorScale = chroma.scale(["#53cf8d", "#f7d283", "#e85151"]);
const amountScale = d3.scaleLog();

const simulation = d3
  .forceSimulation()
  .alphaDecay(0.001)
  .velocityDecay(0.3)
  .force("collide", d3.forceCollide(radius))
  .force(
    "x",
    d3.forceX(d => d.focusX)
  )
  .force(
    "y",
    d3.forceY(d => d.focusY)
  )
  .stop();

const drag = d3.drag();

class Expenses extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};

    simulation.on("tick", this.forceTick);
    drag
      .on("start", this.dragStart)
      .on("drag", this.dragExpense)
      .on("end", this.dragEnd);
  }

  componentDidMount() {
    const { width, height } = this.props;
    this.container = d3.select("#expenses");

    xScale.range([margin.left, width * 1.2]);
    yScale.range([height - margin.bottom, margin.top]);

    this.transformData();
    this.renderDayCircles();
    this.renderWeeksBars();
    this.renderCircles();

    simulation
      .nodes(this.props.expenses)
      .alpha(0.9)
      .restart();
  }

  componentDidUpdate() {
    this.transformData();
    // this.renderCircles();

    simulation
      .nodes(this.expenses)
      .alpha(0.9)
      .restart();
  }

  transformData = () => {
    const { width, height, expenses, selectedWeek } = this.props;
    const daysRadius = 80;

    const weekExtent = d3.extent(expenses, d =>
      d3.timeWeek.floor(moment(d.date, "DD/MM/YYYY"))
    );
    yScale.domain(weekExtent);

    const weeks = d3.timeWeek.range(
      weekExtent[0],
      d3.timeWeek.offset(weekExtent[1], 1)
    );

    this.weeks = _.map(weeks, week => {
      return {
        week,
        x: margin.left,
        y: yScale(week) + height * 1.3
      };
    });

    let selectedWeekRadius = (width - margin.left - margin.right) / 3;

    const perAngle = Math.PI / 6;
    let currentWeekX = angle =>
      selectedWeekRadius * Math.cos(angle) + width / 2;
    let currentWeekY = angle =>
      selectedWeekRadius * Math.sin(angle) + height * 0.1;

    this.days = _.map(daysOfWeekArray, d => {
      let [dayOfWeek, label] = d;
      let angle = Math.PI - perAngle * dayOfWeek;
      let x = currentWeekX(angle);
      let y = currentWeekY(angle);
      return {
        label,
        date: d3.timeDay.offset(selectedWeek, dayOfWeek),
        radius: daysRadius,
        x,
        y
      };
    });

    this.expenses = _.chain(expenses)
      .groupBy(d => {
        return d3.timeWeek.floor(moment(d.date, "DD/MM/YYYY"));
      })
      .map((expenses, week) => {
        week = new Date(week);
        return _.map(expenses, exp => {
          var dayOfWeek = new Date(moment(exp.date, "DD/MM/YYYY")).getDay();

          var focusX = xScale(dayOfWeek);
          var focusY = yScale(week) + height * 1.25;

          if (week.getTime() === selectedWeek.getTime()) {
            var angle = Math.PI - perAngle * dayOfWeek;

            focusX = currentWeekX(angle);
            focusY = currentWeekY(angle);
          }

          return Object.assign(exp, {
            focusX,
            focusY
          });
        });
      })
      .flatten()
      .value();

    const amountExtent = d3.extent(expenses, d => d.amount);
    amountScale.domain(amountExtent);

    return this.expenses;
  };

  renderCircles = () => {
    //draw circles
    this.circles = this.container.selectAll(".expense").data(this.expenses);

    //exit
    this.circles.exit().remove();

    //enter + update
    this.circles = this.circles
      .enter()
      .append("circle")
      .attr("class", "expense")
      .attr("r", radius)
      .attr("fill-opacity", 0.25)
      .attr("stroke-width", 3)
      .call(drag)
      .merge(this.circles)
      .attr("fill", d => {
        return colorScale(amountScale(d.amount));
      })
      .attr("stroke", d => colorScale(amountScale(d.amount)))
      .attr("title", d => d.date);
  };

  renderDayCircles = () => {
    const fontSize = 14;

    const days = this.container
      .selectAll(".day")
      .data(this.days)
      .enter()
      .append("g")
      .attr("class", "day")
      .attr("transform", d => "translate(" + [d.x, d.y] + ")");

    const circles = days
      .append("circle")
      .attr("r", d => d.radius)
      .attr("fill", "#ccc")
      .attr("fill-opacity", 0.25);

    const text = days
      .append("text")
      .attr("y", d => d.radius + fontSize)
      .attr("text-anchor", "middle")
      .attr("dy", ".35em")
      .text(d => d.label);
  };

  renderWeeksBars = () => {
    const rectHeight = 10;

    const weeksSvg = this.container
      .selectAll(".week")
      .data(this.weeks)
      .enter()
      .append("g")
      .attr("class", "week")
      .attr("transform", d => "translate(" + [d.x, d.y] + ")");

    weeksSvg
      .append("rect")
      .attr("width", xScale(6))
      .attr("height", rectHeight)
      .attr("y", -rectHeight * 3.5)
      .attr("fill", "#ccc")
      .attr("fill-opacity", 0.25);

    const weekFormat = d3.timeFormat("%d/%m");
    weeksSvg
      .append("text")
      .attr("text-anchor", "end")
      .attr("dy", -rectHeight * 2.5)
      .attr("dx", -20)
      .text(d => weekFormat(d.week));
  };

  forceTick = () => {
    this.circles.attr("cx", d => d.x).attr("cy", d => d.y);
  };

  dragStart = () => {
    simulation.alphaTarget(0.3).restart();
    d3.event.subject.fx = d3.event.subject.x;
    d3.event.subject.fy = d3.event.subject.y;
  };

  dragExpense = () => {
    const { categories } = this.props;

    this.dragged = null;
    d3.event.subject.fx = d3.event.x;
    d3.event.subject.fy = d3.event.y;

    //object being dragged
    const expense = d3.event.subject;

    const expenseX = d3.event.x;
    const expenseY = d3.event.y;

    //drag onto categories
    categories.map(category => {
      let { x, y, radius } = category;
      if (
        x - radius < expenseX &&
        expenseX < x + radius &&
        y - radius < expenseY &&
        expenseY < y + radius
      ) {
        this.dragged = { expense, category };
      }
    });

    //drag onto days
    this.days.map(day => {
      let { x, y, radius } = day;
      if (
        x - radius < expenseX &&
        expenseX < x + radius &&
        y - radius < expenseY &&
        expenseY < y + radius
      ) {
        this.dragged = { expense, day };
      }
    });
  };

  dragEnd = () => {
    const { linkToCategory, editDate } = this.props;
    if (!d3.event.active) simulation.alphaTarget(0);
    d3.event.subject.fx = null;
    d3.event.subject.fy = null;

    if (this.dragged) {
      const { expense, category, day } = this.dragged;
      if ("category" in this.dragged) {
        linkToCategory(expense, category);
      } else if ("day" in this.dragged) {
        if (
          new Date(moment(expense.date, "DD/MM/YYYY")).getTime() !==
          new Date(day.date).getTime()
        ) {
          editDate(expense, day);
        }
      }
    }

    this.dragged = null;
  };

  render() {
    const { width, height } = this.props;

    return <g id="expenses" />;
  }
}

export default Expenses;
