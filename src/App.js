import React from "react";
import "./App.css";

import * as d3 from "d3";
import moment from "moment";
import expenses from "./data.json";
import Expenses from "./visualisations/Expenses";
import Categories from "./visualisations/Categories";

const width = 1500;
const height = 600;

const maxWeek = d3.max([
  ...expenses.map(exp => d3.timeWeek.floor(moment(exp.date, "DD/MM/YYYY")))
]);

const minWeek = d3.min([
  ...expenses.map(exp => d3.timeWeek.floor(moment(exp.date, "DD/MM/YYYY")))
]);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      expenses: [],
      categories: [
        { name: "Groceries", expenses: [], total: 0 },
        { name: "Restaurant", expenses: [], total: 0 }
      ],
      selectedWeek: null
    };
  }

  componentDidMount() {
    //default selected week will be the most recent week
    let selectedWeek = maxWeek;

    //temp id
    expenses.map(exp => {
      return Object.assign(exp, {
        id: Math.random() * 10000
      });
    });

    this.setState({
      selectedWeek,
      expenses,
      loading: false
    });
  }

  handleWeekSelect = (prev = false) => {
    const offset = prev ? -1 : 1;
    const selectedWeek = d3.timeWeek.offset(this.state.selectedWeek, offset);
    this.setState({
      selectedWeek
    });
  };

  linkToCategory = (expense, category) => {
    const { categories, links } = this.state;

    Object.assign(
      category,
      category.expenses.some(exp => exp.id === expense.id)
        ? {
            expenses: category.expenses.filter(exp => exp.id !== expense.id),
            total: category.total - expense.amount
          }
        : {
            expenses: [...category.expenses, expense],
            total: category.total + expense.amount
          }
    );

    categories.some(cat => {
      if (cat.name === category.name) {
        return Object.assign(cat, category);
      }
    });

    this.setState({ categories });
  };

  editDate = (expense, day) => {
    const { expenses } = this.state;
    Object.assign(
      expenses,
      expenses.some(exp => {
        if (exp.id === expense.id) {
          Object.assign(exp, {
            date: moment(day.date).format("DD/MM/YYYY")
          });
        }
      })
    );
    this.setState({ expenses });
  };

  render() {
    const { selectedWeek, loading, categories } = this.state;

    const formattedSelectedWeek = d3.timeFormat("%B %d, %Y")(selectedWeek);

    const hasNextWeek = !(
      selectedWeek && selectedWeek.getTime() === maxWeek.getTime()
    );
    const hasPreviousWeek = !(
      selectedWeek && selectedWeek.getTime() === minWeek.getTime()
    );

    const links = [];
    categories.map(cat => {
      cat.expenses.map(exp => {
        if (
          d3.timeWeek.floor(moment(exp.date, "DD/MM/YYYY")).getTime() ===
          selectedWeek.getTime()
        ) {
          links.push({
            source: exp,
            target: cat
          });
        }
      });
    });

    return (
      <div>
        {loading ? (
          <>Loading</>
        ) : (
          <>
            <h3 className="week-select">
              {hasPreviousWeek ? (
                <span
                  onClick={() => this.handleWeekSelect(true)}
                  className="arrow"
                >
                  ←
                </span>
              ) : (
                ""
              )}
              Week of {formattedSelectedWeek}
              {hasNextWeek ? (
                <span onClick={() => this.handleWeekSelect()} className="arrow">
                  →
                </span>
              ) : (
                ""
              )}
            </h3>
            <svg
              width={width}
              height={height}
              viewBox={`0 0 ${width} ${height * 2.3}`}
            >
              <Categories categories={categories} width={width} links={links} />
              <Expenses
                {...this.state}
                width={width}
                height={height}
                categories={categories}
                linkToCategory={this.linkToCategory}
                editDate={this.editDate}
              />
            </svg>
          </>
        )}
      </div>
    );
  }
}

export default App;
